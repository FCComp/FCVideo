//Copyright 2018 Huawei Technologies Co.,Ltd.

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger,HWLiveCaptureType) {
    HWLiveCaptureAudio,         //< capture only audio
    HWLiveCaptureVideo,         //< capture onlt video
    HWLiveInputAudio,           //< only audio (External input audio)
    HWLiveInputVideo,           //< only video (External input video)
};


///< 用来控制采集类型（可以内部采集也可以外部传入等各种组合，支持单音频与单视频,外部输入适用于录屏，无人机等外设介入）
typedef NS_ENUM(NSInteger,HWLiveCaptureTypeMask) {
    HWLiveCaptureMaskAudio = (1 << HWLiveCaptureAudio),                                 ///< only inner capture audio (no video)
    HWLiveCaptureMaskVideo = (1 << HWLiveCaptureVideo),                                 ///< only inner capture video (no audio)
    HWLiveInputMaskAudio = (1 << HWLiveInputAudio),                                     ///< only outer input audio (no video)
    HWLiveInputMaskVideo = (1 << HWLiveInputVideo),                                     ///< only outer input video (no audio)
    HWLiveCaptureMaskAll = (HWLiveCaptureMaskAudio | HWLiveCaptureMaskVideo),           ///< inner capture audio and video
    HWLiveInputMaskAll = (HWLiveInputMaskAudio | HWLiveInputMaskVideo),                 ///< outer input audio and video(method see pushVideo and pushAudio)
    HWLiveCaptureMaskAudioInputVideo = (HWLiveCaptureMaskAudio | HWLiveInputMaskVideo), ///< inner capture audio and outer input video(method pushVideo and setRunning)
    HWLiveCaptureMaskVideoInputAudio = (HWLiveCaptureMaskVideo | HWLiveInputMaskAudio), ///< inner capture video and outer input audio(method pushAudio and setRunning)
    HWLiveCaptureDefaultMask = HWLiveCaptureMaskAll                                     ///< default is inner capture audio and video
};

/// 音频码率 (默认96Kbps)
typedef NS_ENUM (NSUInteger, HWLiveAudioBitRate) {
    /// 32Kbps 音频码率
    HWLiveAudioBitRate_32Kbps = 32000,
    /// 64Kbps 音频码率
    HWLiveAudioBitRate_64Kbps = 64000,
    /// 96Kbps 音频码率
    HWLiveAudioBitRate_96Kbps = 96000,
    /// 128Kbps 音频码率
    HWLiveAudioBitRate_128Kbps = 128000,
    /// 默认音频码率，默认为 96Kbps
    HWLiveAudioBitRate_Default = HWLiveAudioBitRate_96Kbps
};

/// 音频采样率 (默认44.1KHz)
typedef NS_ENUM (NSUInteger, HWLiveAudioSampleRate){
    /// 16KHz 采样率
    HWLiveAudioSampleRate_16000Hz = 16000,
    /// 44.1KHz 采样率
    HWLiveAudioSampleRate_44100Hz = 44100,
    /// 48KHz 采样率
    HWLiveAudioSampleRate_48000Hz = 48000,
    /// 默认音频采样率，默认为 44.1KHz
    HWLiveAudioSampleRate_Default = HWLiveAudioSampleRate_44100Hz
};

///  Audio Live quality（音频质量）
typedef NS_ENUM (NSUInteger, HWLiveAudioQuality){
    /// 低音频质量 audio sample rate: 16KHz audio bitrate: numberOfChannels 1 : 32Kbps  2 : 64Kbps
    HWLiveAudioQuality_Low = 0,
    /// 中音频质量 audio sample rate: 44.1KHz audio bitrate: 96Kbps
    HWLiveAudioQuality_Medium = 1,
    /// 高音频质量 audio sample rate: 44.1MHz audio bitrate: 128Kbps
    HWLiveAudioQuality_High = 2,
    /// 超高音频质量 audio sample rate: 48KHz, audio bitrate: 128Kbps
    HWLiveAudioQuality_VeryHigh = 3,
    /// 默认音频质量 audio sample rate: 44.1KHz, audio bitrate: 96Kbps
    HWLiveAudioQuality_Default = HWLiveAudioQuality_High
};

@interface HWLiveAudioConfiguration : NSObject<NSCoding, NSCopying>

/// 默认音频配置
+ (instancetype)defaultConfiguration;
/// 音频配置
+ (instancetype)defaultConfigurationForQuality:(HWLiveAudioQuality)audioQuality;

#pragma mark - Attribute
///=============================================================================
/// @name Attribute
///=============================================================================
/// 声道数目(default 2)
@property (nonatomic, assign) NSUInteger numberOfChannels;
/// 采样率
@property (nonatomic, assign) HWLiveAudioSampleRate audioSampleRate;
/// 码率
@property (nonatomic, assign) HWLiveAudioBitRate audioBitrate;
/// flv编码音频头 44100 为0x12 0x10
@property (nonatomic, assign, readonly) char *asc;
/// 缓存区长度
@property (nonatomic, assign,readonly) NSUInteger bufferLength;

@end
