//
//  LiveI003MainController.h
//  LiveI003
//   
//  Created by ZhouYou on 2018/9/18.
//  Copyright © 2018年 ZhouYou. All rights reserved.
//

#import <TMSDK/TMSDK.h>

//视频首页,支持配置导航栏隐藏与否，是否是青海项目
@interface LiveI003MainController : TMViewController

@property (nonatomic , assign) BOOL isLiveNavHidden;

@end

//音频首页，支持配置导航栏隐藏与否，是否是青海项目
@interface LiveI003AudoMainController : LiveI003MainController

@end
