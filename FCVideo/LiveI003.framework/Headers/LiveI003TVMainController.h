//
//  LiveI003TVMainController.h
//  LiveI003
//
//  Created by ZhouYou on 2019/6/25.
//  Copyright © 2019 DengJie. All rights reserved.
//

#import <TMSDK/TMSDK.h>

/** 直播、电视、广播*/
NS_ASSUME_NONNULL_BEGIN

@interface LiveI003TVMainController : TMViewController

@end

NS_ASSUME_NONNULL_END
