//
//  LiveI003FullScreenVc.h
//  LiveI003
//
//  Created by omni－appple on 2018/12/21.
//  Copyright © 2018年 ZhouYou. All rights reserved.
//

#import <TMSDK/TMSDK.h>

@interface LiveI003FullScreenVc : TMViewController
@property (nonatomic, assign) UIDeviceOrientation deviceOrientation;

@property (nonatomic,copy)void (^callbackScreenUp)(UIDeviceOrientation Or);
/// 横屏进入  1  右边  横屏  2  左边   横屏
@property (nonatomic,assign) NSInteger  type;
@end
